#!/bin/sh
# Copyright 2022 Helmut Grohne <helmut@subdivi.de>
# SPDX-License-Identifier: MIT
#
# This is a mmdebstrap customize hook that enables and configures
# systemd-networkd on various Debian releases.

set -eu

TARGET=$1

SYSTEMD_VERSION=$(dpkg-query --root "$TARGET" -f '${Version}' -W systemd)

if test "${MMDEBSTRAP_MODE:-}" = chrootless; then
	systemctl --root "$TARGET" enable systemd-networkd.service
else
	chroot "$TARGET" systemctl enable systemd-networkd.service
fi

{
	echo '[Match]'
	echo 'Name=en*'
	if dpkg --compare-versions "$SYSTEMD_VERSION" lt 220-7~; then
		echo 'Name=eth*'
	fi

	echo '[Network]'
	echo 'DHCP=yes'

	if dpkg --compare-versions "$SYSTEMD_VERSION" lt 249; then
		# This anchor is included by default since bullseye. Fails DNSSEC
		# validation when missing.
		echo 'DNSSECNegativeTrustAnchors=home.arpa'
	fi
	echo '[DHCP]'
	echo 'UseDomains=yes'
} >"$TARGET/etc/systemd/network/20-wired.network"
