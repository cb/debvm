#!/bin/sh
# Copyright 2023 Johannes Schauer Marin Rodrigues <josch@debian.org>
# SPDX-License-Identifier: MIT
#
# Add a non-root user, add them to the sudo group and use the same authorized
# ssh keys as the root user.
#
#  - the new user is called "user"
#  - no password required for login
#  - requires the passwd and coreutils packages installed inside the chroot
#  - adds the new user to the sudo group if it exists
#  - ~/.ssh/authorized_keys files is copied from root user if it exists
#
# Example usage:
#
#     $ debvm-create -k ~/.ssh/id_rsa.pub -- --hook-dir=.../useraddhook --include sudo
#     $ debvm-run -s 8022
#     $ ssh -l user -p 8022 127.0.0.1 whoami
#     user
#     $ ssh -l user -p 8022 127.0.0.1 sudo whoami
#     root
#

set -eu

chroot "$1" useradd --home-dir /home/user --create-home --shell /bin/bash user
chroot "$1" passwd --delete user
if chroot "$1" getent group sudo >/dev/null; then
	chroot "$1" usermod --append --groups sudo user
fi
if [ -e "$1"/root/.ssh/authorized_keys ]; then
	chroot "$1" install -o user -g user -m 700 -d /home/user/.ssh
	chroot "$1" install -o user -g user -t /home/user/.ssh /root/.ssh/authorized_keys
fi
